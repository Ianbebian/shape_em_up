﻿using UnityEngine;
using System.Collections;

public class PlayerKeyboard : MonoBehaviour {
    public float speed = 20.0f; // Player speed
    //public GameObject projectile; //Laser projectile prefab goes here
	public Rigidbody2D laserRigidbody2D; //Rigidbody2D goes here. Configurable Projectile shooter.
	public int playerHp = 1;
	public double projectileCooldown = 0.3f; // cooldown in seconds
	public enum shootingMode {single, scatter, beam};
	public shootingMode currentShotMode;
	public GameObject explosionPrefab;
	GameObject attachedExplosion = null;
	Rigidbody2D attachedLaserClone = null;
	public bool shield = false;
    private Vector2 dir; // Directional destination
    private Vector2 pos; // Current position
    private Vector2 point; // The destination to move towards
	//public bool isEnemy = true;
    private RaycastHit2D hit; // The point that is scanned ahead to find
	public Sprite img1 , img2;
	private double nextFire = -1.0f; // Total time from the start (-1) till the next shooting time

    // Use this for initialization
	void Start() 
	{
		gameObject.transform.position = new Vector2(0f, -16f);
		GameObject.Find ("GameController").GetComponent<B_GameGui>().ShotMode.text = "Shot: Single";	
		currentShotMode = shootingMode.single; // default set at single
		shield = true;
		Debug.Log ("Sheild on");
		StartCoroutine(spawnSheild());
	}

    void Update()
    {
        if (GameObject.Find("Player(Clone)").GetComponent<PlayerKeyboard>().shield == true)
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = img2;
        }
        else if (GameObject.Find("Player(Clone)").GetComponent<PlayerKeyboard>().shield == false)
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = img1;
        }

        // Enables smooth ship like movement
        float vx = Input.GetAxis("Horizontal") * speed * Time.deltaTime;
        float vy = Input.GetAxis("Vertical") * speed * Time.deltaTime;
        transform.Translate(new Vector3(vx, vy, 0));
		// Shoot
		if (Input.GetKey("space")) // SPACE
		{
			if(Time.time >= nextFire){ // Is cooldown finished?
				nextFire = Time.time + projectileCooldown; //Reset cooldown
				shoot(currentShotMode); // shoots with the current mode
			}
		}
    }

	public void deductLife(){
		GameObject GameControllerObject= GameObject.Find("GameController");
		B_GameGui deductLife= GameControllerObject.gameObject.GetComponent<B_GameGui>();
		Instantiate (explosionPrefab, transform.position, transform.rotation);
		deductLife.LoseLife ();
		Destroy(this.gameObject);
	}

	public IEnumerator spawnSheild(){
		yield return new WaitForSeconds (2f);
		Debug.Log ("Sheild off");
		shield = false;
	}

	/**
     * Moves player the closer to the destination via the rigid body
     * (Old Movement Method)
     */ 
    void moveTo(Vector2 dest)
    {
        point = Vector2.MoveTowards(transform.position, dest, speed);
        GetComponent<Rigidbody2D>().MovePosition(point);
    }

    // Is the direction valid and not in a collider?
    bool valid(Vector2 direction) 
    {
        pos = transform.position;
        hit = Physics2D.Linecast(pos + (direction * 2), pos);
        return (hit.collider == GetComponent<Collider2D>());
    }

	// Shoots projectiles
	public void shoot(shootingMode shot) 
	{
		switch (shot)
		{
		case shootingMode.single:
			// Creates and shoots a configurable projectile
			attachedLaserClone = (Rigidbody2D) Instantiate(
				laserRigidbody2D, transform.position + (2 * Vector3.up), transform.rotation);
			break;

		case shootingMode.scatter:
			for (int i = 0; i < 5; i++ ){
				// Creates and shoots a configurable projectile
				Rigidbody2D laserClone2 = (Rigidbody2D) Instantiate(
                    laserRigidbody2D, transform.position + (2 * Vector3.up), transform.rotation);
				// spread like a folding fan
				laserClone2.GetComponent<laserScript>().angle = -45f + (22.5f * (float) i); 
			}
			break;

		case shootingMode.beam:
			Vector3 pos = transform.position+ (2 * Vector3.up) + (3 * Vector3.left); // start on the left most
            // shoot 5 times
			for (int i = 0; i < 5; i++ ){
                pos = pos + Vector3.right; // move the laser 1 unit to the right
                Vector3 p = pos + (Vector3.down * Mathf.Abs(i - 2)); // Create arrow like formation
				// Creates and shoots a configurable projectile
				Rigidbody2D laserClone3 = (Rigidbody2D) Instantiate(
					laserRigidbody2D, p, transform.rotation);

			}
			break;

		default:
			currentShotMode = shootingMode.single;
			break;
		}
	}

	void OnTriggerEnter2D(Collider2D coll){	
		Debug.Log("111");
		if(coll.CompareTag("BeamPowerUp")){
			GameObject.Find ("GameController").GetComponent<B_GameGui>().ShotMode.text = "Shot: Beam";
			currentShotMode = shootingMode.beam;
		}
		if(coll.CompareTag("ScatterPowerUp")){
			GameObject.Find ("GameController").GetComponent<B_GameGui>().ShotMode.text = "Shot: Scatter";
			currentShotMode = shootingMode.scatter;
		}
	}

}
