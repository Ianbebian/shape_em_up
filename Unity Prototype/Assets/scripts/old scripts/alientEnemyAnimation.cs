﻿using UnityEngine;
using System.Collections;

/**
 *  This script is an extra script for animating aliens enemies.
 *  Low on backlog priority.
 */

public class alientEnemyAnimation : MonoBehaviour {
    

    private float index; // current index of the animation
    private Vector3 offset;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        index = Mathf.FloorToInt(Time.time * 12.0f) % 4f;
        offset = new Vector3(index / 4.0f, 1f, 1f);

		//transform.localScale = transform.localScale * offset;
		// Used for planes with textures
        //renderer.material.SetTextureScale("_MainTex", size);
        //renderer.material.SetTextureOffset("_MainTex", offset);
	}
}
