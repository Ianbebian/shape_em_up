﻿using UnityEngine;
using System.Collections;

public class FloaterScript : laserScript {

	// Overides
	void Update () {
		transform.Translate(0, lasSpeed * Time.deltaTime, 0);

		//==========================================================================
		// Extra Floater functionality 
		//==========================================================================
		transform.Rotate(Vector3.right * Time.deltaTime * lasSpeed);
	}

	// Overides
    void OnTriggerEnter2D(Collider2D otherCollider)
    {
		// Destroys player
		PlayerKeyboard player = otherCollider.gameObject.GetComponent<PlayerKeyboard>();
		GameObject playerLife= GameObject.Find("Player(Clone)");
		PlayerKeyboard reduceLife= playerLife.gameObject.GetComponent<PlayerKeyboard>();
		if (player != null && isEnemyShot)// PLAYER.LOSE LIFE
		{
			if(reduceLife.shield == false){
				Instantiate(explosionPrefab, transform.position, transform.rotation);
				reduceLife.deductLife ();
				Destroy(gameObject);
			}           
		}
		
		// When shot by player, it should die in contact with anything but the player.
		if (!isEnemyShot && otherCollider.tag == "enemy")
		{
			Instantiate(explosionPrefab, transform.position, transform.rotation);
			Destroy(gameObject);
		}

		//==========================================================================
		// Extra Floater functionality 
		//==========================================================================
		laserScript otherLaser = otherCollider.gameObject.GetComponent<laserScript>();
	    if (otherLaser != null && isEnemyShot)
        {
            // if the laser is not an enemy shot?
            if (!otherLaser.isEnemyShot)
                Instantiate(explosionPrefab, transform.position, transform.rotation);
                Destroy(this.gameObject);
        }
    }
}
